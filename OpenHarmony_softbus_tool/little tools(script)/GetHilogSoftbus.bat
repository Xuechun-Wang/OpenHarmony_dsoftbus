@echo off
cls
setlocal enabledelayedexpansion

set LogDir=%cd%\log
for /f "delims=" %%i in ('hdc list targets') do (
echo %%i | findstr Empty
if !ERRORLEVEL!==1 call:GetLog %%i
)

if !ERRORLEVEL!==0 pause
EXIT /b 0

:GetLog
set /a hour=%time:~0,2%*1
if %hour% LSS 10 set hour=0%hour%
set LogFileName=Hilog_%date:~0,4%%date,~5,2%%date:~8,2%_%hour%%time:~3,2%%time:~6,2%_%1

set HilogCmd="hilog -G 200M;hilog"
set HilogDsoftbusCmd="hilog -Q domainoff 0xD0015c0;hilog -D 0xD0015c0"

start /min "%1" cmd /k "hdc -t %1 shell %HilogDsoftbusCmd% > %LogDir%\%LogFileName%.txt"
goto:eof
