@echo off
cls
setlocal enabledelayedexpansion

for /f "delims=" %%i in ('hdc list targets') do (
echo %%i | findstr Empty
if !ERRORLEVEL!==1 call:Shell %%i
)

if !ERRORLEVEL!==0 pause
EXIT /b 0

:Shell
start "%1" cmd /k "hdc -t %1 shell"
goto:eof
