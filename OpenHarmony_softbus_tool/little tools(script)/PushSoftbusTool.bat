@echo off
cls
setlocal enabledelayedexpansion

for /f "delims=" %%i in ('hdc list targets') do (
echo %%i | findstr Empty
if !ERRORLEVEL!==1 call:PushFile %%i
)

pause
EXIT /b 0

:PushFile

echo ------Device: %1

hdc -t %1 shell "rm -rf /data/softbus_tool"

hdc -t %1 shell "mkdir /data/softbus_tool"

hdc -t %1 file send %cd%\softbus_tool /data/softbus_tool

hdc -t %1 shell "chmod a+x /data/softbus_tool/softbus_tool"

goto:eof
