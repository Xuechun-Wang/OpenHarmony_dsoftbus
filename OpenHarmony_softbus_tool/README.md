#  openharmony_softbus_tool

## 介绍

分布式软总线Api调用测试工具~

## 软件架构

软件架构说明

## 编译说明

1. 将softbus_tool目录代码下载到本地，拷贝到OpenHarmony源码\foundation\communication\dsoftbus\tools下

2. 修改\foundation\communication\dsoftbus\tools\BUILD.gn添加 

   修改前：deps = [ ":SoftBusDumpDeviceInfo" ] 

   修改后：deps = [ ":SoftBusDumpDeviceInfo", **"softbus_tool:softbus_tool"** ]

3. 单编softbus_tool编译命令： 

   **RK3568:** 

   ./build.sh --product-name rk3568 --ccache --build-target softbus_tool 

   **RK3516:**

    ./build.sh --product-name Hi3516DV300 --ccache --build-target softbus_tool

4. 编译结果： 

   \out\rk3568\communication\dsoftbus_standard\softbus_tool

## 使用说明



## 参与贡献




