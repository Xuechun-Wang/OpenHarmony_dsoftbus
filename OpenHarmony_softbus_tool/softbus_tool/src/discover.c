/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "discover.h"

#include "common.h"
#include "discovery_service.h"
#include "ohos_bt_gap.h"

#define MAX_FOUND_DEVICE_NUM 32

typedef struct {
    uint32_t num;
    DeviceInfo dev[MAX_FOUND_DEVICE_NUM];
} DeviceList;

static sem_t g_publishSem;
static sem_t g_subscribeSem;
static bool g_needShowFoundDevice = false;
static bool g_isStartDiscoverySucc = false;
static DeviceList g_deviceList = { 0, {0} };

static void OnPublishSuccess(int publishId)
{
    printf(">>>OnPublishSuccess publishId = %d.\n", publishId);
    sem_post(&g_publishSem);
}

static void OnPublishFail(int publishId, PublishFailReason reason)
{
    printf(">>>OnPublishFail publishId = %d, reason = %d.\n", publishId, reason);
    sem_post(&g_publishSem);
}

void D_PublishService(void)
{
    sem_init(&g_publishSem, 0, 0);
    PublishInfo info = {
        .publishId = 1,
        .medium = BLE,
        .mode = DISCOVER_MODE_ACTIVE,
        .freq = MID,
        .capability = "dvKit",
        .capabilityData = (unsigned char *)"capdata1",
        .dataLen = 9,
    };
    info.publishId = GetInputNumber("Please input publish id:");
    info.medium = GetInputNumber("Please input publish medium(0 - AUTO, 1 - BLE, 2 - COAP):");

    IPublishCallback cb = {
        .OnPublishSuccess = OnPublishSuccess,
        .OnPublishFail = OnPublishFail,
    };
    int ret = PublishService(PKG_NAME, &info, &cb);
    if (ret != 0) {
        printf("PublishService fail, ret = %d.\n", ret);
        sem_destroy(&g_publishSem);
        return;
    }
    sem_wait(&g_publishSem);
    sem_destroy(&g_publishSem);
}

void D_UnPublishService(void)
{
    int publishId = GetInputNumber("Please input publish id:");
    int ret = UnPublishService(PKG_NAME, publishId);
    if (ret != 0) {
        printf("UnPublishService fail, ret = %d.\n", ret);
    }
}

static void ClearFoundDevices(void)
{
    (void)memset_s(&g_deviceList, sizeof(DeviceList), 0, sizeof(DeviceList));
}

static bool RecordFoundDevice(const DeviceInfo *device)
{
    for (int i = 0; i < MAX_FOUND_DEVICE_NUM; i++) {
        if (memcmp(device->addr[0].info.ble.bleMac,
            g_deviceList.dev[i].addr[0].info.ble.bleMac, MAC_SIZE) == 0) {
            return false;
        }
    }
    (void)memcpy_s(&g_deviceList.dev[g_deviceList.num], sizeof(DeviceInfo), device, sizeof(DeviceInfo));
    g_deviceList.num++;
    if (g_deviceList.num >= MAX_FOUND_DEVICE_NUM) {
        g_deviceList.num = 0;
    }
    return true;
}

static void OnDeviceFound(const DeviceInfo *device)
{
    if (device == NULL || !g_needShowFoundDevice || !RecordFoundDevice(device)) {
        return;
    }
    printf(">>>OnDeviceFound: DevId - %s  |  BLE MAC - "MAC_STR"\n",
        device->devId, MAC_ADDR(&(device->addr[0].info.ble.bleMac[0])));
}

static void OnDiscoverFailed(int subscribeId, DiscoveryFailReason reason)
{
    printf(">>>OnDiscoverFailed subscribeId = %d, reason = %d.\n", subscribeId, reason);
    sem_post(&g_subscribeSem);
}

static void OnDiscoverySuccess(int subscribeId)
{
    printf(">>>OnDiscoverySuccess subscribeId = %d.\n", subscribeId);
    g_isStartDiscoverySucc = true;
    sem_post(&g_subscribeSem);
}

void D_StartDiscovery(void)
{
    sem_init(&g_subscribeSem, 0, 0);
    SubscribeInfo info = {
        .subscribeId = 1,
        .medium = BLE,
        .mode = DISCOVER_MODE_ACTIVE,
        .freq = MID,
        .capability = "dvKit",
        .capabilityData = (unsigned char *)"capdata1",
        .dataLen = 9,
        .isSameAccount = false,
        .isWakeRemote = false,
    };
    info.subscribeId = GetInputNumber("Please input subscribe id:");
    info.medium = GetInputNumber("Please input subscribe medium(0 - AUTO, 1 - BLE, 2 - COAP):");

    IDiscoveryCallback cb = {
        .OnDeviceFound = OnDeviceFound,
        .OnDiscoverFailed = OnDiscoverFailed,
        .OnDiscoverySuccess = OnDiscoverySuccess,
    };
    ClearFoundDevices();
    g_needShowFoundDevice = true;
    g_isStartDiscoverySucc = false;
    int ret = StartDiscovery(PKG_NAME, &info, &cb);
    if (ret != 0) {
        printf("StartDiscovery fail, ret = %d.\n", ret);
        sem_destroy(&g_subscribeSem);
        return;
    }
    sem_wait(&g_subscribeSem);
    if (g_isStartDiscoverySucc) {
        WaitInputEnter("Found Devices shown below, please input ENTER key to break.\n");
        g_needShowFoundDevice = false;
    }
    sem_destroy(&g_subscribeSem);
}

void D_StopDiscovery(void)
{
    int subscribeId = GetInputNumber("Please input subscribe id:");
    int ret = StopDiscovery(PKG_NAME, subscribeId);
    if (ret != 0) {
        printf("StopDiscovery fail, ret = %d.\n", ret);
    }
}

void D_GetBtMac(void)
{
    unsigned char addr[MAC_SIZE] = {0};
    if (!GetLocalAddr(addr, MAC_SIZE)) {
        printf("GetLocalAddr fail\n");
        return;
    }
    printf("BT MAC - ["MAC_STR"]\n", MAC_ADDR(addr));
}

